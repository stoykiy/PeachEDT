# PeachEDT

A small Peach Pit editor, created as a side project during my internship. 
Features:
- Fast template creation for peach pits
- Inserting blocks of XML code with RMK
- Syntax highlighting
- Running and debugging the pit file directly from GUI

Impression of the GUI:

![alt tag](https://raw.githubusercontent.com/remyjaspers31337/PeachEDT/master/peachedt.png)
