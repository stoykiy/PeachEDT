#-------------------------------------------------
#
# Project created by QtCreator 2014-12-23T09:01:41
#
#-------------------------------------------------

QT       += core gui xml xmlpatterns

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = PeachEdt
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    highlighter.cpp \
    texteditor.cpp

HEADERS  += mainwindow.h \
    highlighter.h \
    messagehandler.h \
    texteditor.h

FORMS    += mainwindow.ui

RESOURCES += \
    resources.qrc
