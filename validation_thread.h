#ifndef VALIDATION_THREAD_H
#define VALIDATION_THREAD_H
#include <QThread>

class validation_thread : public QThread
{
    Q_OBJECT
public:
    validation_thread();
    ~validation_thread();
private:
    void run();
};

#endif // VALIDATION_THREAD_H
