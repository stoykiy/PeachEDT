#include <QtCore>
#include <Qurl>
#include <QXmlSchema>
#include <QXmlSchemaValidator>
#include "validation_thread.h"
#include "messagehandler.h"
#include "mainwindow.h"

validation_thread::validation_thread()
{
    qDebug() << "constructing validation thread" << endl;
}

validation_thread::~validation_thread()
{
    qDebug() << "destructing validation thread" << endl;
}

void validation_thread::run()
{
    {
        while(true)
        {
            QUrl url("qrc:/resources/peach.xsd");

            const QByteArray instanceData = completingTextEdit->toPlainText().toUtf8();

            MessageHandler messageHandler;

            QXmlSchema schema;
            schema.setMessageHandler(&messageHandler);

            schema.load(url);

            bool errorOccurred = false;
            if (!schema.isValid()) {
                errorOccurred = true;
            } else {
                QXmlSchemaValidator validator(schema);
                if (!validator.validate(instanceData))
                    errorOccurred = true;
            }

            if (errorOccurred) {
                statusLabel->setText(messageHandler.statusMessage());
                moveCursor(messageHandler.line(), messageHandler.column());
            } else {
                statusLabel->setText(tr("Validation successful"));
            }

            const QString styleSheet = QString("QstatusLabel {background: %1; padding: 3px}")
                                              .arg(errorOccurred ? QColor(Qt::red).lighter(160).name() :
                                                                   QColor(Qt::green).lighter(160).name());
            statusLabel->setStyleSheet(styleSheet);
            qDebug() << "Running function threaded" << QThread::currentThread();
        }
}
}
