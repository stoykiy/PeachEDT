#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QProcess>
#include <QCompleter>
#include <QLabel>
#include "highlighter.h"
#include "texteditor.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void setupEditor();
    void on_actionOpenFile_triggered();
    void on_actionSaveFile_triggered();
    bool validate();
    void textChanged();
    void addActionsToContextMenu();
    void on_actionRun_triggered();
    void on_actionClearText_triggered();
    void on_actionAddDataModel_triggered();
    void on_actionNew_Peach_Pit_triggered();
    void on_actionNight_Day_Mode_triggered(bool checked);

    void on_actionAdd_StateModel_triggered();

private:
    void moveCursor(int line, int column);
    QProcess *proc_console_output;
    QString openfileName;
    Ui::MainWindow *ui;
    QUrl *xsdSchema;
    QLabel *statusLabel;
    Highlighter *highlighter;
    void createMenu();
    QAbstractItemModel *modelFromFile(const QString& fileName);
    QCompleter *completer;
    TextEditor *completingTextEdit;
};

#endif // MAINWINDOW_H
