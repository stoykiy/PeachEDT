#include <QtGui>
#include <QtXmlPatterns>
#include <QDomDocument>
#include <QFileDialog>
#include <QLabel>
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "highlighter.h"
#include "messagehandler.h"
#include "texteditor.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setupEditor();
    addActionsToContextMenu();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::setupEditor()
 {
     QFont font;
     font.setFamily("Courier");
     font.setFixedPitch(true);
     font.setPointSize(10);

     statusLabel = new QLabel;
     statusLabel->setTextFormat(Qt::RichText);
     statusBar()->addWidget(statusLabel);
     completingTextEdit = new TextEditor;
     completer = new QCompleter(this);
     completer->setModel(modelFromFile(":resources/wordlist.txt"));
     completer->setModelSorting(QCompleter::CaseInsensitivelySortedModel);
     completer->setCaseSensitivity(Qt::CaseInsensitive);
     completer->setWrapAround(false);
     completingTextEdit->setLineWrapMode(QTextEdit::NoWrap);
     completingTextEdit->setCompleter(completer);
     completingTextEdit->setContextMenuPolicy(Qt::ActionsContextMenu);

     setCentralWidget(completingTextEdit);
     completingTextEdit->setFont(font);
     completingTextEdit->setPalette(Qt::white);

     highlighter = new Highlighter(completingTextEdit->document());

     QFile file("mainwindow.h");
     if (file.open(QFile::ReadOnly | QFile::Text))
         completingTextEdit->setPlainText(file.readAll());

 }

void MainWindow::on_actionOpenFile_triggered()
{
    openfileName = QFileDialog::getOpenFileName(this,
        tr("Open File"), "/", tr("Peach Pit Files (*.xml)"));

    QDomDocument doc("domdoc");
    QFile file(openfileName);
    if (!file.open(QIODevice::ReadOnly))
        return;
    if (!doc.setContent(&file)) {
        file.close();
        return;
    }
    file.close();

    QString xml = doc.toString();
    completingTextEdit->setPlainText(xml);
    statusLabel->setText("File opened succesfully");
    qDebug() << QDir::currentPath() ;
}

void MainWindow::on_actionSaveFile_triggered()
{
    QString savefileName = QFileDialog::getSaveFileName(this, tr("Save File"),
                               openfileName,
                               tr("Peach Pit File (*.xml)"));
    QString xml = completingTextEdit->toPlainText();
    QFile file(savefileName);
        file.open(QIODevice::WriteOnly | QIODevice::Text);
        QTextStream out(&file);
        out << xml;
        statusLabel->setText("File saved succesfully.");
        file.close();
}

 bool MainWindow::validate()
 {
     QUrl url("qrc:/resources/peach.xsd");

     const QByteArray instanceData = completingTextEdit->toPlainText().toUtf8();

     MessageHandler messageHandler;

     QXmlSchema schema;
     schema.setMessageHandler(&messageHandler);

     schema.load(url);

     bool errorOccurred = false;
     if (!schema.isValid()) {
         errorOccurred = true;
     } else {
         QXmlSchemaValidator validator(schema);
         if (!validator.validate(instanceData))
             errorOccurred = true;
     }

     if (errorOccurred) {
         statusLabel->setText(messageHandler.statusMessage());
         moveCursor(messageHandler.line(), messageHandler.column());
         return false;
     } else {
         statusLabel->setText(tr("Validation successful"));
         return true;
     }

     const QString styleSheet = QString("QstatusLabel {background: %1; padding: 3px}")
                                       .arg(errorOccurred ? QColor(Qt::red).lighter(160).name() :
                                                            QColor(Qt::green).lighter(160).name());
     statusLabel->setStyleSheet(styleSheet);
 }

 void MainWindow::textChanged()
 {
     completingTextEdit->setExtraSelections(QList<QTextEdit::ExtraSelection>());
 }

 void MainWindow::moveCursor(int line, int column)
 {
     completingTextEdit->moveCursor(QTextCursor::Start);
     for (int i = 1; i < line; ++i)
         completingTextEdit->moveCursor(QTextCursor::Down);

     for (int i = 1; i < column; ++i)
         completingTextEdit->moveCursor(QTextCursor::Right);

     QList<QTextEdit::ExtraSelection> extraSelections;
     QTextEdit::ExtraSelection selection;

     const QColor lineColor = QColor(Qt::red).lighter(160);
     selection.format.setBackground(lineColor);
     selection.format.setProperty(QTextFormat::FullWidthSelection, true);
     selection.cursor = completingTextEdit->textCursor();
     selection.cursor.clearSelection();
     extraSelections.append(selection);

     completingTextEdit->setExtraSelections(extraSelections);
     completingTextEdit->setFocus();
 }

void MainWindow::on_actionRun_triggered()
{
    validate();
    if(validate())
    {
    QString xml = completingTextEdit->toPlainText();
    QFile file(QDir::currentPath().append("/temp.xml"));
        file.open(QIODevice::WriteOnly | QIODevice::Text);
        QTextStream out(&file);
        out << xml;
        statusLabel->setText("Pit validated succesfully. Starting in Peach... ");
        file.close();

    QString program = QDir::currentPath().append("/peach/peach.exe");
    QStringList arguments;
    arguments << QDir::currentPath().append("/temp.xml");
    proc_console_output->startDetached(program, arguments);
    }
    else{
         statusLabel->setText("Unable to validate pit.");
    }
}


void MainWindow::on_actionClearText_triggered()
{
    completingTextEdit->clear();
}

void MainWindow::on_actionAddDataModel_triggered()
{
    completingTextEdit->insertPlainText("<DataModel name="">\n\t<String name=""/>\n\t<Blob name=""/>\n</DataModel>");
}

void MainWindow::on_actionNew_Peach_Pit_triggered()
{
    QDomDocument doc("domdoc");
    QFile file(":/resources/HelloWorld.xml");
    if (!file.open(QIODevice::ReadOnly))
        return;
    if (!doc.setContent(&file)) {
        file.close();
        return;
    }
    file.close();

    QString xml = doc.toString();
    completingTextEdit->setPlainText(xml);
    statusLabel->setText("Starting a fresh Peach Pit!");
}

void MainWindow::on_actionNight_Day_Mode_triggered(bool checked)
{
    if(checked)
    {
        completingTextEdit->setStyleSheet("QTextEdit{background-color: white}");
    }
    else
    {
        completingTextEdit->setStyleSheet("QTextEdit{background-color: #333}");
    }
}

void MainWindow::addActionsToContextMenu()
{
    completingTextEdit->addAction(ui->actionClearText);
    completingTextEdit->addAction(ui->actionAddDataModel);
    completingTextEdit->addAction(ui->actionAdd_StateModel);
}

QAbstractItemModel *MainWindow::modelFromFile(const QString& fileName)
 {
     QFile file(fileName);
     if (!file.open(QFile::ReadOnly))
         return new QStringListModel(completer);

 #ifndef QT_NO_CURSOR
     QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));
 #endif
     QStringList words;

     while (!file.atEnd()) {
         QByteArray line = file.readLine();
         if (!line.isEmpty())
             words << line.trimmed();
     }

 #ifndef QT_NO_CURSOR
     QApplication::restoreOverrideCursor();
 #endif
     return new QStringListModel(words, completer);
 }

void MainWindow::on_actionAdd_StateModel_triggered()
{
    QString statemodelString = "\n<StateModel name=\"\">";
        statemodelString.append("\n\t");
        statemodelString.append("<State name=\"\">");
        statemodelString.append("\n\t");
        statemodelString.append("</State>");
        statemodelString.append("\n</StateModel>");
    completingTextEdit->insertPlainText(statemodelString);
}
